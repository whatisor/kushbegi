package com.eyedaptic.kushbegi

class EditableModel{
    var value: Float = 1.0f
    var min: Float = 0.0f
    var max: Float = 2.0f
    var defaultVal: Float = 1.0f

}