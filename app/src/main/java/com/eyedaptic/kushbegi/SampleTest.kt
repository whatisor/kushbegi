
//
// just some sample usage -- not really runnable code

package com.eyedaptic.kushbegi

fun main() {

  // get a list o fnames of available glasses types
  val allGlassesNames = GlassesDatabase.allGlasses.keys
  // populate a gui, let the user choose which type..
  // ....
  // ....
  val chosenType = "ODG R-7" // we would want to get this from a gui choice, or from the actual glasses that connected
  val glasses = GlassesDatabase[chosenType]

  // now that we know what kind of glasses we are using, we can create a device model

  val deviceModel = DeviceModel(glasses)

  // next we need to sync the gui with the device model
  // ...
  // ...

  

}