
package com.eyedaptic.kushbegi

class DeviceModel(glasses: Glasses) {

  // these fields are tied to the gui -- eventually need to add setters that
  // enforce the constraints and cause changes to be propagated to the glasses
  // if they are connected

  // EXAMPLE: when portableEnabled=false, we want to disable the gui for for "Center"
  //          without changing centerX or centerY; but when its true, centerEnabled
  //          can be either true or false
  var name:String = "Device name"
  var portalEnabled = true
  var portalInner = glasses.defaultPortalInner
  var portalOuter = glasses.defaultPortalOuter
  var portalMag = glasses.defaultPortalMag

  var centerEnabled  = false
  var centerX = 0.0
  var centerY = 0.0

  var lineEnabled = false
  var lineY = 0.0

  var britextEnabled = false
  var britextC = 0.0

  var contrastEnabled = false
  var contrastC = 1.0

  // NOTE: when warpShareCenter is true, force warpX and warpY to be the same as centerX and centerY

  var warpEnabled = false
  var warpShareCenter = true
  var warpX = 0.0
  var warpY = 0.0

  var gridEnabled = false
  var gridSize = glasses.defaultGridSize // INTEGER
  var gridX = 0 // INTEGER
  var gridY = 0 // INTEGER

  var scotomaEnabled = false
  var scotomaSize = glasses.scaleScotoma
  var scotomaX = 0.0
  var scotomaY = 0.0
  var scotomaOpacity = 1.0

  var blurEnabled = false

  //

  var connected = false // will be set TRUE when a connection to remote glasses exists

  //

  fun pull() {

    // TO BE WRITTEN: pull() gets a full set of parameters from the connected pair of glasses,
    //    and sets all the fields listed above.  after this function is called, the gui is responsible
    //    for making sure that it reflects the new state

  }

  fun push() {

    // TO BE WRITTEN: push() converts the set of fields above into a set of paraemeters that
    // will cause the remotely connected glasses to have the same state [this will require
    // some consolidation and computations]

  }

}
