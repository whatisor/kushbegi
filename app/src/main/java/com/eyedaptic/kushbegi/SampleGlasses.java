package com.eyedaptic.kushbegi;

import org.jetbrains.annotations.NotNull;

public class SampleGlasses extends Glasses {
    public SampleGlasses(@NotNull String name, @NotNull String description) {
        super(name, description);
    }
}
