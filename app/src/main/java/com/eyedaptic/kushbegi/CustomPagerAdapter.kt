package com.eyedaptic.kushbegi

import android.bluetooth.BluetoothClass
import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eyedaptic.kushbegi.databinding.CenterBinding
import com.eyedaptic.kushbegi.databinding.MainBinding
import com.eyedaptic.kushbegi.databinding.PortalBinding

class CustomPagerAdapter(val device:DeviceModel,val layoutInflater:LayoutInflater,private val list: List<Int>) : PagerAdapter() {

    override fun isViewFromObject(v: View, `object`: Any): Boolean {
        // Return the current view
        return v === `object` as View
    }


    override fun getCount(): Int {
        // Count the items and return it
        return list.size
    }



    override fun instantiateItem(parent: ViewGroup, position: Int): Any {
        // Get the view from pager page layout
        lateinit var binding : ViewDataBinding
            when (list[position]) {
                R.id.btnPortal -> { binding = DataBindingUtil.inflate(layoutInflater,R.layout.portal,parent,false)}
                R.id.btnCenter ->{ binding = DataBindingUtil.inflate(layoutInflater,R.layout.center,parent,false)}
                R.id.btnLine ->{binding = DataBindingUtil.inflate(layoutInflater,R.layout.line,parent,false)}
                R.id.btnBritext->{binding = DataBindingUtil.inflate(layoutInflater,R.layout.britext,parent,false)}
                R.id.btnConstrast->{binding = DataBindingUtil.inflate(layoutInflater,R.layout.contrast,parent,false)}
            }
        val view:ViewDataBinding = DataBindingUtil.inflate(layoutInflater,list[position],parent,false)
        // Add the view to the parent
        parent?.addView(view.root)

        // Return the view
        return view.root
    }


    override fun destroyItem(parent: ViewGroup, position: Int, `object`: Any) {
        // Remove the view from view group specified position
        parent.removeView(`object` as View)
    }
}