package com.eyedaptic.kushbegi

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.util.Log
import android.view.LayoutInflater

import android.view.View
import android.widget.SeekBar
import com.eyedaptic.kushbegi.databinding.MainBinding
import kotlinx.android.synthetic.main.leftbar.view.*
import kotlinx.android.synthetic.main.main.view.*
import kotlinx.android.synthetic.main.params.view.*
import kotlinx.android.synthetic.main.topbar.view.*


class MainActivity  : AppCompatActivity() {
    lateinit var binding : MainBinding
    lateinit var editable : EditableModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.main)
        val device:DeviceModel = DeviceModel( SampleGlasses("Test","Test"))
        val connection:ConnectionModel = ConnectionModel()
        val user:UserModel = UserModel()
        editable = EditableModel()
        binding.device = device
        binding.connection = connection
        binding.user = user
        binding.editable = editable

        // Initialize a list of page
        val params = listOf(R.layout.portal,R.layout.center,R.layout.line,R.layout.britext,R.layout.contrast)

        // Initialize a new pager adapter instance with list
        val adapter = CustomPagerAdapter(device,LayoutInflater.from(this),params)

        // Finally, data bind the view pager widget with pager adapter??? too deep access
        //binding.main.center.leftbar.parametersContainer.params.parameters.viewPager.adapter = adapter;
        binding.main.viewPager.adapter = adapter;


        binding.executePendingBindings()
    }
    fun updatePager(position:Int){
        binding.main.viewPager.currentItem = position
    }
    fun updatePagerEvent(btn:View){
        when (btn.id) {
            R.id.btnPortal ->updatePager(0)
            R.id.btnCenter ->updatePager(1)
            R.id.btnLine ->updatePager(2)
            R.id.btnBritext->updatePager(3)
            R.id.btnConstrast->updatePager(4)
        }
    }

    fun setupBinding(value:Float,min:Float,max:Float,defaultVal:Float){

    }
    fun showSlider(btn:View){
        Log.e("showSlider",""+btn.id);
        when (btn.id) {
            R.id.btnInner ->setupBinding()
            R.id.btnOuter ->setupBinding()
            R.id.btnMag ->setupBinding()
            R.id.btnX->setupBinding()
            R.id.btnY->setupBinding()
            R.id.btnY->setupBinding()
            R.id.btnBritextC->setupBinding()
            R.id.btnContrastC->setupBinding()
        }
    }



    //Editor
    fun updateValue(p0: CharSequence?, p1: Int, p2: Int, p3: Int){
        Log.e("updateValue",p0.toString());
    }
    fun setMin(){
        Log.e("setMin","");
    }
    fun setMax(){
        Log.e("setMax","");
    }
    fun setDefault(){
        Log.e("setMin","");
    }
    fun updateValueFromSB(seekBar: SeekBar, progress: Int, fromUser: Boolean){
        Log.e("updateValueFromSB",progress.toString());
    }
}