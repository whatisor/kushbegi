
package com.eyedaptic.kushbegi

//
// Glasses - gives characteristics for a specific type of glasses
//

abstract class Glasses(val name : String, val description : String) {

  open val width = 1280
  open val height = 720

  open val hFovDegrees = 27.0
  open val vFovDegrees = 15.0
  open val dFovDegrees = 30.0

  open val maxPortalInner = 4.0
  open val maxPortalOuter = 4.0
  open val maxPortalMag = 10.0
  open val defaultPortalInner = 1.0
  open val defaultPortalOuter = 1.4
  open val defaultPortalMag = 1.0

  open val maxCenterX = 1280.0/720.0
  open val maxCenterY = 1.0

  open val maxLineY = 1.0

  open val minBritextC = 0.0
  open val maxBritextC = 10.0
  open val lowBritextC = 5.0
  open val medBritextC = 10.0

  open val maxContrastC = 4.0

  open val maxWarpSize = 1.0

  open val minGridSize = 32
  open val defaultGridSize = 64
  open val maxGridSize = 256

  open val scaleScotoma = 1.0

  init {
    GlassesDatabase.register(this)
  }

}

//
// GlassesDatabase - each subclass of Glasses automatically registers itself, so a client
// program can obtain all available types from GlassesDatabase.allGlasses
//

object GlassesDatabase {

  val allGlasses : HashMap<String,Glasses> = hashMapOf()

  fun register(glasses: Glasses) {
    allGlasses[glasses.name] = glasses
  }

  operator fun get(name: String) : Glasses = allGlasses[name]!!

}

//
// OdgR7Glasses - Osterhout Design Group R-7
//

object OdgR7Glasses : Glasses("ODG R7", "") {

  // no changes ot baseline

}


